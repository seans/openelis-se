// MasterMix

function MasterMix()
{
	// get number of filled wells
	
	// calculate reagent number
	
	// perform reagent calculations
	
	// sum totals
}

var filledWells = 2;

for (i=1; i <= 17; i++) {
	var tempValue = String(this.getField("well_label_" + i).value).trim();
	if (!(tempValue == '' || tempValue == null)) {
		filledWells++;
	}
}

this.getField("Reagents").value = filledWells;

this.getField("rxn_total").value = this.getField("rxn_1").value + this.getField("rxn_2").value + this.getField("rxn_3").value + this.getField("rxn_4").value;

this.getField("mix_1").value = filledWells * this.getField("rxn_1").value;
this.getField("mix_2").value = filledWells * this.getField("rxn_2").value;
this.getField("mix_3").value = filledWells * this.getField("rxn_3").value;
this.getField("mix_4").value = filledWells * this.getField("rxn_4").value;

this.getField("mix_total").value = this.getField("mix_1").value + this.getField("mix_2").value + this.getField("mix_3").value + this.getField("mix_4").value;


/*

var a = this.getField("quantity_wells_page" + this.pageNum);
var b = this.getField("quantity_samples_page" + this.pageNum);
var c = this.getField("reagent_media_quantity_1_page" + this.pageNum);
var d = this.getField("reagent_media_quantity_2_page" + this.pageNum);
var e = this.getField("reagent_media_quantity_3_page" + this.pageNum);
var f = this.getField("reagent_media_quantity_4_page" + this.pageNum);
var g = this.getField("reagent_media_quantity_5_page" + this.pageNum);
var h = this.getField("reagent_media_quantity_6_page" + this.pageNum);
var i = this.getField("reagent_media_quantity_7_page" + this.pageNum);
var j = this.getField("reagent_media_name_1_page" + this.pageNum);
var k = this.getField("reagent_media_name_2_page" + this.pageNum);
var l = this.getField("reagent_media_name_3_page" + this.pageNum);
var m = this.getField("reagent_media_name_4_page" + this.pageNum);
var n = this.getField("reagent_media_name_5_page" + this.pageNum);
var o = this.getField("reagent_media_name_6_page" + this.pageNum);
var p = this.getField("reagent_media_name_7_page" + this.pageNum);
b.value = Math.ceil(a.value * 0.1) + a.value;
if(j.value==''||j.value==null){c.value==null}else c.value=b.value *0.5;
if(k.value==''||k.value==null){d.value==null}else d.value=b.value *12.5;
if(l.value==''||l.value==null){e.value==null}else e.value=b.value *7;
if(m.value==''||m.value==null){f.value==null}else f.value=b.value *7;
if(n.value==''||n.value==null){g.value==null}else g.value=b.value *7;
if(o.value==''||o.value==null){h.value==null}else h.value=b.value *7;
if(p.value==''||p.value==null){i.value==null}else i.value=b.value *7;

*/