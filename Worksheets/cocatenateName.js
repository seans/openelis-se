function concatenateName(firstName, lastName) {
	var test = 0;
	firstName= firstName.toString();
	lastName = lastName.toString();

	if (firstName!= "") test += 1;
	if (lastName!= "") test += 2;

	if (test == 0) return "";
	if (test == 1) return firstName;
	if (test == 2) return lastName;
	if (test == 3) return lastName + ", " + firstName;
}
